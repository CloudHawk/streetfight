import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, health, attack, defense, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const defenceElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const healthElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes : { src: source }});
  
  // show fighter name, attack, defense, health, image

  nameElement.innerText = 'name: ' + name;
  attackElement.innerText = 'attack: ' + attack;
  defenceElement.innerText = 'defence: ' + defense;
  healthElement.innerText = 'health: ' + health;
  fighterDetails.append(nameElement, attackElement, defenceElement, healthElement, imageElement);

  return fighterDetails;
}
