import {showModal} from "./modal";
import {createElement} from "../helpers/domHelper";

export  function showWinnerModal(fighter) {
  // show winner name and image
    const title = 'Winner';
    const bodyElement = createWinnerDetails(fighter);
    showModal({ title, bodyElement });
}

function createWinnerDetails(fighter) {
    const { name, source } = fighter;

    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
    const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes : { src: source }});

    nameElement.innerText = 'name: ' + name;
    fighterDetails.append(imageElement, nameElement);

    return fighterDetails;
}
