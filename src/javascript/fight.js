export function fight(firstFighter, secondFighter) {
    let dmg;
    let firstHP = firstFighter.health;
    let secondHP = secondFighter.health;
    while (true){
        dmg = getDamage(firstFighter, secondFighter);
        secondHP -= dmg;
        if(secondHP < 0) {
           break;
        }
        dmg = getDamage(secondFighter, firstFighter);
        firstHP -= dmg;
        if(firstHP < 0) {
            break;
        }
    }
    if(firstHP < 0) {
        return secondFighter;
    } else {
        return firstFighter;
    }
  // return winner
}

export function getDamage(attacker, enemy) {
    const hit = getHitPower(attacker);
    const block = getBlockPower(enemy);
        return hit - block > 0
            ? hit - block
            : 0;

  // damage = hit - block
  // return damage 
}

export function getHitPower(fighter) {
    return fighter.attack * (Math.random() + 1)
  // return hit power
}

export function getBlockPower(fighter) {
    return fighter.defense * (Math.random() + 1)
  // return block power
}
